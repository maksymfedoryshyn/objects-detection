from vidimg_utils.object_detector.detector import Detector
from vidimg_utils.object_detector.detection import Detection
import cv2
import numpy as np

class FaceDetector(Detector):
    """Class responsible for objects detection. Current implementation uses dnn module from OpenCV, and pre-trained caffe models."""

    def __init__(self, path_to_proto, path_to_model, supported_classes):
        """Creates instance of the class and loads DNN topology and weights

        Parameters
        ----------
        :param path_to_proto: path to file with network topology
        :type  path_to_proto: string
        :param path_to_model: path to file with weights for the network layers
        :type  path_to_model: string
        :param supported_classes: array of strings, which contains types of all the supported classes of objects, that this
                                  network can detect and recognise. If array is empty, or variable is not an array of strings,
                                  exception will be thrown
        :type  supported_classes: array
        """
        if not isinstance(supported_classes, list):
            raise ValueError("Supported classes parameter is not a list of strings")
        self.supported_classes = supported_classes
        self.scale = 1.0
        self.n_network = cv2.dnn.readNetFromCaffe(path_to_proto, path_to_model)

    
    def recognise(self, image, classes = None, confidence_threshold = 0.5):
        """Overrides Detector.recognise(). We work here with the model pre-trained just for face recignition, so param classes is ignored"""
        # convert image to a blob
        blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), self.scale,
            (300, 300), (104.0, 177.0, 123.0))
     
        # pass the blob through the network and obtain the detections and predictions
        self.n_network.setInput(blob)
        all_detections = self.n_network.forward()

        # iterating over detections
        (h, w) = image.shape[:2]
        detected_objects = []
        for i in range(0, all_detections.shape[2]):
            # extract the confidence (i.e., probability) associated with the prediction
            confidence = all_detections[0, 0, i, 2]
            # filter out weak detections by ensuring the `confidence` is
            # greater than the minimum confidence
            if confidence < confidence_threshold:
                continue
            # compute the (x, y)-coordinates of the bounding box for the object
            box = all_detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (start_x, start_y, end_x, end_y) = box.astype("int")
            detected_objects.append(Detection(0, confidence, start_x, start_y, end_x, end_y))

        return detected_objects