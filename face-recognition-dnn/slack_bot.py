import logging
import os
import requests
import time


class SlackStatus:
    """Class, which incapsulates latest state for the slack ("active" vs. "away") and some simple logic of its calculation
    """
    
    def __init__(self, delay):
        """Init slack status object

        Parameters
        ----------
        :param delay: time, during which changes to user's presense don't change user's state (anti-jittering)
        :type  delay: number
        """
        self.present = True
        self.delay = delay
        self.presence_changed_at = time.time()

    def person_present(self, presence):
        """Calculates user's status.

        Parameters
        ----------
        :param presence: current value for user's presence (received from the latest frame)
        :type  presence: boolean
        
        Returns
        ----------
        True - if user is present
        False - if user is away
        None - if user's presense hasn't changed
        """
        current_time = time.time()
        last_seen_present = self.present
        # if presence changed
        if last_seen_present ^ presence:
            self.present = presence
            self.presence_changed_at = current_time
        elif current_time - self.presence_changed_at > self.delay:
            return presence
        return None


class SlackBot:
    """Slack bot, which process detections and generates requests to the slack if user's presense has been changed
    """

    def __init__(self, faces_count, slack_status):
        """Constructs slack bot instance

        Parameters
        ----------
        :param faces_count: expected number of detections for "active" slack status 
                            in other words how many faces have to be detected in the video frame for user status to be changed to "active")
        :type  faces_count: number
        :param slack_status: instance for the slack status class to calculate user's status
        :type  slack_status: SlackStatus
        """
        self.faces_count = faces_count
        self.slack_status = slack_status
        self.slack_token = os.getenv("SLACK_TOKEN")
        self.slack_channel = os.getenv("SLACK_CHANNEL")
        self.last_status = True


    def process(self, detections):
        """Process face detections and update user's status if changed

        :param detections: list of face detections (from input video stream)
        """
        if len(detections) == 0 or len(detections) == self.faces_count:
            is_user_active = self.slack_status.person_present(len(detections) == self.faces_count)
            # if status changed
            if is_user_active != None and self.last_status != is_user_active:
                self.update_user_status(is_user_active)
                self.post_message(is_user_active)
                self.last_status = is_user_active


    def update_user_status(self, is_user_active):
        """Prepare and execute request to slack to update user's status

        :param is_user_active: indicates wether user is active or away
        :type  is_user_active: boolean
        """
        logging.info("Changing user activity to " + str(is_user_active))
        (url, headers) = self.prepare_request("users.profile.set")
        data = ""
        if is_user_active == True:
            data = '''{
                  "profile": {
                    "status_text": "",
                    "status_emoji": "",
                    "status_expiration": 0
                  }
                }'''
        else:
            data = '''{
                  "profile": {
                    "status_text": "Maks is not at his desk",
                    "status_emoji": ":running:",
                    "status_expiration": 0
                  }
                }'''
        response = requests.post(url, headers=headers, data=data)

    def post_message(self, is_user_active):
        """Prepare and execute request to send message to the team's slack channel (f.e. "Hey team, I'm away!")
        It is more convenient to change status AND post message to the team's channel, than just change status.

        :param is_user_active: indicates wether user is active or away
        :type  is_user_active: boolean
        """
        logging.info("Posting message to the channel")
        (url, headers) = self.prepare_request("chat.postMessage")
        data = ""
        if is_user_active == True:
            data = '''{
                  "channel": "{0}",
                  "text": "Maks is back!"
                }'''
        else:
            data = '''{
                  "channel": "{0}",
                  "text": "Maks is not at his desk. He will be back soon."
                }'''
        response = requests.post(url, headers=headers, data=data.format(self.slack_channel))


    def prepare_request(self, method):
        """Prepare request template to post requests to slack

        :param method: request method
        :type  method: string
        """
        return "https://slack.com/api/" + method, {'Content-Type': 'application/json; charset=utf-8', 'Authorization': "Bearer " + self.slack_token}
