# tested with opencv 4.5.5

from vidimg_utils.screen.utils  import bring_python_window_foreground_if_havent_before
import logging
import cv2
import sys
import os
import platform
import semver


def is_cv_version_smaller_than_4_5_1():
    """In OpenCV v. 4.5.1 some tracing functions were moved to the "legacy" package. We will have to check OpenCV version to be able to corrctly instantiate trackers.
    For example:  https://stackoverflow.com/questions/67159209/attributeerror-module-cv2-cv2-has-no-attribute-trackermosse-create
    """
    ver = semver.VersionInfo.parse(cv2.__version__);
    return ver.major < 4 or ver.minor < 5 or ver.patch < 1;


def tracking_boundary_box():
    """Returns boundary box to use in the methods. Created for convenience
    """
    return (200, 150, 150, 200);


def show_first_frames_for_calibration(video, bbox):
    """Shows first few frames for initial boundary calibration
    
    For built in tracking algorithms, it is very important to correctly select initial boundaries. 
    If initial boundaries are selected not around the face, tracking algorithm performs very badly.
    So this method shows few first frames and box (passed as an argumen) to check if initial box placement is accurate.
    :param video: input video stream (can be from the camera or pre-recorded video file)
    :param bbox: initial boundary box. It will be displayed over the video playback (to visually verify if it's placed correctly)
    """
    # Exit if video not opened.
    if not video.isOpened():
        logging.error("Could not open video")
        sys.exit()
 
    # Read first frame.
    ok, frame = video.read()
    if not ok:
        logging.error('Cannot read video file')
        sys.exit()
     
    i = 0
    while i < 5:
        i = i + 1
        # Read a new frame
        ok, frame = video.read()
        if not ok:
            break
         
        # Draw bounding box
        p1 = (int(bbox[0]), int(bbox[1]))
        p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
        cv2.rectangle(frame, p1, p2, (255,0,0), 2, 1)
        # Display result
        cv2.imshow("Capturing", frame)
        
        bring_python_window_foreground_if_havent_before()
        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27 : break
    input("Press Enter to continue...")


def calibrate_video_camera():
    """Allows to calibrate inital boundary box for input stream from the camera"""
    video = None
    try:
        video = cv2.VideoCapture(0)
        show_first_frames_for_calibration(video, tracking_boundary_box())
    except:
        if video is not None:
            video.release()
        raise
    video.release()

def track_object(video, tracker_type, initial_boundary_box, output):
    """Track face using specified tracking algorithm (https://ehsangazar.com/object-tracking-with-opencv-fd18ccdd7369)

    :param input: input video stream. Can be stream from the camera or file
    :param tracker_typ - type of the tracking algorithm. Can be one of: BOOSTING, MIL, KCF, TLD, MEDIAFLOW, CSRT, MOSSE
    :type tracker_typ: string
    :param initial_boundary_box: initial boundary box (in this exercise we are targeting face)
    :param output: output file or screen
    """
    if tracker_type == 'BOOSTING':
        tracker = cv2.TrackerBoosting_create() if is_cv_version_smaller_than_4_5_1() else cv2.legacy.TrackerBoosting_create()
    elif tracker_type == 'MIL':
        tracker = cv2.TrackerMIL_create() if is_cv_version_smaller_than_4_5_1() else cv2.legacy.TrackerMIL_create()
    elif tracker_type == 'KCF':
        tracker = cv2.TrackerKCF_create()
    elif tracker_type == 'TLD':
        tracker = cv2.TrackerTLD_create() if is_cv_version_smaller_than_4_5_1() else cv2.legacy.TrackerTLD_create() 
    elif tracker_type == 'MEDIANFLOW':
        tracker = cv2.TrackerMedianFlow_create() if is_cv_version_smaller_than_4_5_1() else cv2.legacy.TrackerMedianFlow_create()
    elif tracker_type == 'CSRT':
        tracker = cv2.TrackerCSRT_create()
    elif tracker_type == 'MOSSE':
        tracker = cv2.TrackerMOSSE_create() if is_cv_version_smaller_than_4_5_1() else cv2.legacy.TrackerMOSSE_create()

    logging.info("Tracking using " + tracker_type + " algorithm")

    # Read first frame.
    ok, frame = video.read()
    if not ok:
        logging.error("Cannot read video file for " + tracker_type + " algorithm")
        return
 
    # Initialize tracker with first frame and bounding box
    # Uncomment the line below to select a different bounding box
    # bbox = cv2.selectROI(frame, False)
    bbox = initial_boundary_box
    ok = tracker.init(frame, bbox)

    while True:
        # Read a new frame
        ok, frame = video.read()
        if not ok:
            logging.error("Error reading video frame!")
            break
         
        # Start timer
        timer = cv2.getTickCount()
 
        # Update tracker
        ok, bbox = tracker.update(frame)
 
        # Calculate Frames per second (FPS)
        fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer);
 
        # Draw bounding box
        if ok:
            # Tracking success
            p1 = (int(bbox[0]), int(bbox[1]))
            p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
            cv2.rectangle(frame, p1, p2, (255,0,0), 2, 1)
        else :
            # Tracking failure
            cv2.putText(frame, "Tracking failure detected", (100,80), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0,0,255),2)
 
        # Display tracker type on frame
        cv2.putText(frame, tracker_type + " Tracker", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50),2);
     
        # Display FPS on frame
        cv2.putText(frame, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2);
 
        # Display result
        cv2.imshow("Tracking", frame)
        if output is not None:
            output.write(frame)
        bring_python_window_foreground_if_havent_before()
 
        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27 : break
    logging.info("Relesing video for " + tracker_type)


def test_all_trackers(): 
    """Runs all tracking algorithms on pre-recorded video."""
    tracker_types = ['BOOSTING', 'MIL','KCF', 'TLD', 'MEDIANFLOW', 'CSRT', 'MOSSE']
    
    for tracker_type in tracker_types:
        video = None
        try:
            video = cv2.VideoCapture("./videos/chaplin.mp4")
            # Exit if video not opened.
            if not video.isOpened():
                logging.error("Could not open video for " + tracker_type + " algorithm")
                continue
            track_object(video, tracker_type, tracking_boundary_box(), None)
        except:
            if video is not None:
                video.release()
            raise


def main(): 
    video = None
    output = None
    try:
        # KCF, CSRT
        video = cv2.VideoCapture(0)
        # Exit if video not opened.
        if not video.isOpened():
            logging.error("Could not open video for " + tracker_type + " algorithm")
            return
        # Default resolutions of the frame are obtained.The default resolutions are system dependent.
        # We convert the resolutions from float to integer.
        frame_width = int(video.get(3))
        frame_height = int(video.get(4))
        output = cv2.VideoWriter('output.avi', cv2.VideoWriter_fourcc('M','J','P','G'), 30, (frame_width,frame_height))
        track_object(video, 'MOSSE', tracking_boundary_box(), output)
    except:
        if video is not None:
            video.release()
        if output is not None:
            output.release()
        raise


# if you want to calibrate boundary box, uncomment line below and comment out call to the main() function
# calibrate_video_camera()


# Run all tracking algorithms on pre-recorded video
# test_all_trackers()


# Run face tracking from live stream (web camera)
main()
