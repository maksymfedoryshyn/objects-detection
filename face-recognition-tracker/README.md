** Face tracking using OpenCV and its built-in tracking algorithms **

As a source, I took ideas from [this blogpost](https://ehsangazar.com/object-tracking-with-opencv-fd18ccdd7369).
I wanted to try and see if OpenCV built-in tracking algorithms are good enough for face recognition and tracking.

In a short summary: tested tracking algorithms ('BOOSTING', 'MIL','KCF', 'TLD', 'MEDIANFLOW', 'CSRT', 'MOSSE') are not good enough, all of them have pretty hight rate of false detections

** Contents **

* `main.py` - main script to play with tracking algorithms. It contains lots of comments, so should be easy to read and understand.
* `videos\chaplin.mp4` - example video file with a person, which can be used as an input stream for face tracking
* `results.avi` - some recorded results for face tracking from the live camera
