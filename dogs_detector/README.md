## Dogs detector 

This simple project uses OpenCV and its DNN module for detecting dogs on the input video stream (either from the file or from the camera).

Inital implementation uses pre-trained YOLOv3 model for objects detection. From the description this is one of the fastest models for objects detection.
On practice it takes about few seconds to process the frame on my laptop (Win11, Intel i5-10300H @ 2.50 Gz, 4 Cores, 16Gb RAM).

Used online material are included into links section below.

### Links

#### YOLO
 * https://github.com/ChenYingpeng/caffe-yolov3
 * https://github.com/pjreddie/darknet
 * https://towardsdatascience.com/yolo-object-detection-with-opencv-and-python-21e50ac599e9

#### Miscellaneous

 * [Dogs breed detector](https://towardsdatascience.com/dog-breed-classification-hands-on-approach-b5e4f88c333e) has some info about how to train the model. Can be interesting [github](https://github.com/stormy-ua/dog-breeds-classification)
 * [Dogs images dataset](http://vision.stanford.edu/aditya86/ImageNetDogs/) for training models from the Stanford Uni
 * Dog/cat classification([1](http://adilmoujahid.com/posts/2016/06/introduction-deep-learning-python-caffe/), [2](https://sites.google.com/a/ku.th/big-data/home/caffe/imagenet/dog-cat-training), [3](https://github.com/adilmoujahid/deeplearning-cats-dogs-tutorial))
 * [Dog breed detector](https://towardsdatascience.com/deep-learning-build-a-dog-detector-and-breed-classifier-using-cnn-f6ea2e5d954a) using ResNet50 [github](https://github.com/RahilBA/Dog-Breed-Classifier)
 * Explanation of how `blobFromImage` works with the [example of the objects clasiffication](https://pyimagesearch.com/2017/11/06/deep-learning-opencvs-blobfromimage-works/) using [GoogleNet](https://pytorch.org/hub/pytorch_vision_googlenet/). It is interesting, however pre-traned GoogleNet models don't have one single "dog" category (they have different breeds), so it doesn't work nicely for my case