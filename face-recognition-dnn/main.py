###################################################
# Face detector using built-in opencv dnn module
# Please see README.md for more details
###################################################

from vidimg_utils.screen.utils  import bring_python_window_foreground_if_havent_before
from vidimg_utils.video.stream  import VideoStream

from face_detector import FaceDetector
from slack_bot     import SlackStatus, SlackBot

import logging
import argparse
import cv2
import imutils


# since we use python3, we can make it cool!
def main(): 
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--prototxt", default="model/v1/deploy.prototxt.txt",
        help="path to Caffe 'deploy' prototxt file")
    ap.add_argument("-m", "--model", default="model/v1/res10_300x300_ssd_iter_140000.caffemodel",
        help="path to Caffe pre-trained model")
    ap.add_argument("-c", "--confidence", type=float, default=0.5,
        help="minimum probability to filter weak detections")
    ap.add_argument("-d", "--delay", type=int, default=5,
        help="time to wait before changing slack status")
    ap.add_argument("-f", "--faces_count", type=int, default=1,
        help="expected number of faces")
    args = vars(ap.parse_args())

    # Opening video stream
    vs = VideoStream(None)
    if vs.start() == False:
        logging.error("Cannot open video stream. Terminating")
        return

    # Creating slack status calculator and bot
    slack_status = SlackStatus(args["delay"])
    slack_bot = SlackBot(args["faces_count"], slack_status)

    # Loading pre-trained model
    face_detector = FaceDetector(args["prototxt"], args["model"], ["face"])

    # "watching" stream until "Esc" button is pressed
    while True:
        # Read and resize a new frame
        ok, frame = vs.frame()
        if not ok:
            logging.error("Error reading video frame!")
            break
        
        frame = imutils.resize(frame, width=400)
        detections = face_detector.recognise(frame, confidence_threshold = args["confidence"])
        
        # loop over the detections
        for detection in detections:
            # draw the bounding box of the face along with the associated
            # probability
            text = "{:.2f}%".format(detection.confidence() * 100)
            y = detection.y0() - 10 if detection.y0() - 10 > 10 else detection.y0() + 10
            cv2.rectangle(frame, (detection.x0(), detection.y0()), (detection.x1(), detection.y1()),
                (0, 0, 255), 2)
            cv2.putText(frame, text, (detection.x0(), y),
                cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)

        # Display frame
        cv2.imshow("Tracking", frame)
        bring_python_window_foreground_if_havent_before()

        # process detections
        slack_bot.process(detections)

        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27 : break

    vs.release()
    cv2.destroyAllWindows()


main()