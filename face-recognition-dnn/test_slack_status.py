import unittest
import time
from slack_bot import SlackStatus


class TestSum(unittest.TestCase):

    delay = 5
    # those tests are a bit dumb, ideally it would be great 
    # to mock time functions and make those tests fast

    def test_switch_to_idle_timeout_tolerated(self):
        # status is active after object instantiation
        slack_status = SlackStatus(self.delay)
        self.assertTrue(slack_status.person_present(False) == None)
        self.assertTrue(slack_status.person_present(False) == None)
        self.assertTrue(slack_status.person_present(False) == None)
        time.sleep(2 * self.delay)
        self.assertFalse(slack_status.person_present(False))

    def test_switch_to_active_timeout_tolerated(self):
        # status is active after object instantiation
        slack_status = SlackStatus(self.delay)
        slack_status.person_present(False) # 1
        time.sleep(2 * self.delay)
        self.assertFalse(slack_status.person_present(False)) #5, 6
        self.assertTrue(slack_status.person_present(True) == None) #1
        self.assertTrue(slack_status.person_present(True) == None) #2 4 fail here
        self.assertTrue(slack_status.person_present(True) == None)

        time.sleep(2 * self.delay)
        self.assertTrue(slack_status.person_present(True))

    def test_jittering_doesnt_happen(self):
        # status is active after object instantiation
        slack_status = SlackStatus(self.delay)
        self.assertTrue(slack_status.person_present(False) == None)
        self.assertTrue(slack_status.person_present(True) == None)
        self.assertTrue(slack_status.person_present(False) == None)
        self.assertTrue(slack_status.person_present(True) == None)
        self.assertTrue(slack_status.person_present(False) == None)
        self.assertTrue(slack_status.person_present(True) == None)
        self.assertTrue(slack_status.person_present(False) == None)
        
        time.sleep(2 * self.delay)
        self.assertFalse(slack_status.person_present(False))
        self.assertTrue(slack_status.person_present(True) == None)
        self.assertTrue(slack_status.person_present(False) == None)
        self.assertTrue(slack_status.person_present(True) == None)
        self.assertTrue(slack_status.person_present(False) == None)
        self.assertTrue(slack_status.person_present(True) == None)
        self.assertTrue(slack_status.person_present(False) == None)
        self.assertTrue(slack_status.person_present(True) == None)

def main():
    unittest.main()

main()