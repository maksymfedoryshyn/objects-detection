REM activate virtual environment

REM for Windows we would need to update execution policy to be able to activate virtual environment
REM It can be done by running command below. Alternatively execution policy can be a part of the activation command itself
REM Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
REM activating virtual environment
REM .\face-recognition-venv\Scripts\activate -ExecutionPolicy RemoteSigned -Scope CurrentUser
.\face-recognition-venv\Scripts\activate
REM install dependencies
pip install -r requirements.txt