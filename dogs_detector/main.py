
import logging
import cv2
import imutils
import numpy as np
import argparse

from vidimg_utils.screen.utils  import bring_python_window_foreground_if_havent_before
from vidimg_utils.video.stream  import VideoStream
from vidimg_utils.object_detector.yolov3_detector import YoloV3ObjectsDetector


def draw_prediction(img, class_id, confidence, x, y, x_plus_w, y_plus_h, classes, colors):
    class_label = str(classes[class_id])
    conflidence_label = "{:.2f}%".format(confidence * 100)
    color = colors[class_id]
    cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)
    cv2.putText(img, conflidence_label, (x-10,y-30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    cv2.putText(img, class_label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)


def main(): 
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-n", "--config", default="models/yolo/yolov3.cfg",
        help="path to model config file")
    ap.add_argument("-m", "--model", default="models/yolo/yolov3.weights",
        help="path to pre-trained model")
    ap.add_argument("-c", "--confidence", type=float, default=0.5,
        help="minimum probability to filter weak detections")
    ap.add_argument("-t", "--classes", default="models/yolo/yolov3.classes",
        help="path to the file with list of all the classes (types of objects) current model can detect")
    args = vars(ap.parse_args())


    # Opening video stream
    vs = VideoStream('videos/2_dogs_1.mp4')
    # vs = VideoStream(None)
    if vs.start() == False:
        logging.error("Cannot open video stream. Terminating")
        return

    # prepare list of classes of detected objects. Later they are used for object highlighting on the output stream
    # first, read list of supported classes from the file
    classes = None
    with open(args["classes"]) as f:
        classes = [line.strip() for line in f.readlines()]
    # then, generate different colors for different classes 
    colors = np.random.uniform(0, 255, size=(len(classes), 3))

    ### Now, set up object detector
    # object detector instance
    object_detector = YoloV3ObjectsDetector(args["model"], args["config"], classes)
    # confidence threshold we are intereste in
    confidence_threshold = args["confidence"]

    # "watching" stream until "Esc" button is pressed
    while True:
        # Read a new frame
        ok, frame = vs.frame()
        if not ok:
            logging.error("Error reading video frame!")
            break
        frame = imutils.resize(frame, width=600)
        # run detections
        detections = object_detector.recognise(frame, ["dog"], confidence_threshold=confidence_threshold)
        
        # draw boxes over detectios
        for detection in detections:
            draw_prediction(frame, detection.class_id(), detection.confidence(), round(detection.x0()), round(detection.y0()), round(detection.x1()), round(detection.y1()), classes, colors)
        
        # Display frame
        cv2.imshow("Tracking", frame)
        bring_python_window_foreground_if_havent_before()
        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27 : break

    vs.release()
    cv2.destroyAllWindows()


def stream_video(file_name):
    # Opening video stream
    vs = VideoStream(file_name)
    # vs = VideoStream(None)
    if vs.start() == False:
        logging.error("Cannot open video stream. Terminating")
        return
    # "watching" stream until "Esc" button is pressed
    while True:
        # Read a new frame
        ok, frame = vs.frame()
        if not ok:
            logging.error("Error reading video frame!")
            break
        # Display frame
        frame = imutils.resize(frame, width=600)
        cv2.imshow("Tracking", frame)
        bring_python_window_foreground_if_havent_before()
        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27 : break
    vs.release()
    cv2.destroyAllWindows()

# stream_video('videos/2_dogs_1.mp4')
main()
