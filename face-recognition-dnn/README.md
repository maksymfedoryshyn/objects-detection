** User presense tracker **

This project was my attempt to play with deep neural networks (dnn) capabilities of the OpenCV library.
Practical task was: by using face recognision functionality of the dnn module, track my presence at the desk, and if I'm not sitting at my desk, 
to update my status in the Slack and set it to "away".

I used [this blog](https://www.pyimagesearch.com/2018/02/26/face-detection-with-opencv-and-deep-learning/) as a startign point and created pretty simple program, which actually works pretty well (demo is attached).
